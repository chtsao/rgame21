---
title: "About"
date: 2021-09-08T12:35:45+08:00
tags: []
categories: [admin]
---
![雷姆](https://i.ytimg.com/vi/2ltofrl7IHo/maxresdefault.jpg)

### 初始設定

這是統計軟體與實務應用2021課程網頁。       Curator：Kno Tsao

資料的取得、面向與規模隨著網路發展超速成長。統計是資料科學中的一個重要主軸。本課程將透過引介統計軟體 R, Rstudio, 以及網站 R bloggers, Github, Kaggle 引發學生在資料科學分析與實作的第一步。教學計畫表 syllabus 部份的資料與內容可能將視課程進行狀況略做調整。

以上是官方的說法。真實的設定是  ——— **R games**. 

有興趣同學也可參考一下[2020版課網](https://chtsao.gitlab.io/krg20/)。


### 課程資訊

* Curator: C. Andy Tsao.  Office: SE A411.  Tel: 3520
* Lectures: 
  * 3D: Mon. **1510**-1700, Wed. **1410**-1500 @ AE A212.
  * Virtual: [GoogleClassRoom](https://classroom.google.com/c/MzkxOTcwNDMyODk5?cjc=cd3cqcp), [Google Meet](https://meet.google.com/bsx-ejeh-azu)
* Office Hours:  Mon. 12:10-13:00, Thr. 15:10-16:00. @ SE A411 or by appointment. (Mentor Hour:  Tue/Thr 1200-1300. 也歡迎來，但若有導生來，則導生優先)
* TA Office Hours
	* 蘇羿豪 <611011102@gms.ndhu.edu.tw><038903517>  
		* 星期二 1600-1700，A408. Or Google_meet：https://meet.google.com/tod-apky-kcb
	* 呂一昕 <610911007@gms.ndhu.edu.tw><038903537> 
		* 星期五 1300-1400，A412.  Or Google_meet: :https://meet.google.com/phz-thda-sxc
* Prerequisites: Intro to Probability, Statistics (taken or taking)

