---
title: "Week 11. Statistical Machine Learning I. Workflow"
date: 2021-11-29T11:24:37+08:00
draft: false
tags: [machine learning, boosting, neural network]
---
![](https://miro.medium.com/max/1147/1*mJUJgr8ljq97BL7RU7C_SQ.jpeg)

#### Prelude

* **Former  A408 Grads **
  *  Sean's [Titianic in R](https://www.kaggle.com/osspringfall/titanic-in-r-ndhu), [Predicting Molecular Properties Competition](https://www.kaggle.com/c/champs-scalar-coupling) at [Kaggle](https://www.kaggle.com/)  
  * Yahan's [Food Ingredient Map and Textmining in R](https://enjoy-life-with-helen.netlify.app/post/in-r-resolve-web-crawler-issue/)
* [Lessons from 2 Million Machine Learning Models on Kaggle](https://www.kdnuggets.com/2015/12/harasymiv-lessons-kaggle-machine-learning.html)

## Data Analysis Workflow 
Workflow for CEM (Clean, Explore, Model) part of Data Analysis. Recall

1. Problem defining
	*  What's the problem you want to solve/address? 
	*  Why is it important and what is its impact?
2. Question formulation
	* Domain questions
	* Stat/ML questions
3. Data preparation: import, cleansing/tidying, transforming
4. EDA: Exploratory data analysis
5. Feature engineering and selection
6. Model building, selection, ensembling
7. Prediction and (pseudo)validation


## The problem

Compare and contrast with statistical lm/glm problems.

### A toy example

```{r}
library(MASS)   # Load library for FDA: qda and lda
library(e1071)  # Load library for svm

table(iris$Species)  # Species distribution

msvm <- svm(Species~., data=iris) # Fit iris by SVM
mlda <- lda(Species~., data=iris) # Fit iris by LDA

plda<-predict(mlda,iris)
names(plda)
table(plda$class,iris$Species)    # Confusion matrix

psvm<-predict(msvm)
table(psvm, iris$Species)         # Confusion matrix

# More detailed comparison. Load library for confusionMatrix
library(caret)
confusionMatrix(plda$class,iris$Species)
confusionMatrix(psvm, iris$Species)    
```
So it's easy. That's all? .... Well, not really...

### More realistic examples 

1. Rework [Step-by-step](https://chtsao.gitlab.io/krg20/re.step-by-step.nb.html) (R *k*notebook)
2. [A Complete Tutorial to learn Data Science in R from Scratch](https://www.analyticsvidhya.com/blog/2016/02/complete-tutorial-learn-data-science-scratch/), [Analytics Vidhya](https://www.analyticsvidhya.com/blog/author/avcontentteam/)
3. [Your First ML project in R Step by Step](https://machinelearningmastery.com/machine-learning-in-r-step-by-step/)

### References

1. [An Introduction to Machine Learning with R](https://lgatto.github.io/IntroMachineLearningWithR/) (Gatto 2020)
2. [Hands-On Machine Learning with R](https://bradleyboehmke.github.io/HOML/)  (Boehmke and Greenwell 2020)
3. [Chapter 15. Machine Learning](https://bookdown.org/martin_monkman/DataScienceResources_book/ml.html) of [Data Science with R: A Resource Compendium](https://bookdown.org/martin_monkman/DataScienceResources_book/) (Monkman 2020).
4. [An Introduction to Statistical Learning with Applications in R](http://faculty.marshall.usc.edu/gareth-james/ISL/).   James, Witten, Hastie and Tibshirani (2013).
5. [Code for Workshop: Introduction to Machine Learning with R](https://www.shirin-glander.de/2018/06/intro_to_ml_workshop_heidelberg/) from [Shirin's playgRound](https://shirinsplayground.netlify.app/)

