---
title: "Game 1. Iris Data"
date: 2021-10-13T08:41:10+08:00
draft: false
categories: [note, R, team, game]
tags: [data.frame, data visualization, iris]
---
### Game Init

* Team: 10/24 (Sun) 23:59 請將隊名，隊員名單 email 至我信箱。並開始找尋、探索 project 主題與資料。標題：Rg21. 隊名. 主題. 內容：隊員名單，主題初探。
* Topic and Material
* 伺服器：協作平台/網站 (github, gitlab, bitbucket, kaggle, [hackMD](https://hackmd.io/#)), 發表網站 (blog, youtube, 巴哈姆特, kaggle, etc)

## Iris

Fisher's Iris data set is one of the most famous data sets. ([wiki](https://en.wikipedia.org/wiki/Iris_flower_data_set), [Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Iris), [kaggle](https://www.kaggle.com/arshid/iris-flower-dataset))

![](https://ssar2017kno.files.wordpress.com/2017/02/irisvar.png)

# [Iris data analysis example in R](http://www.slideshare.net/thoi_gian/iris-data-analysis-with-r)
* data.frame, plot, summary; View, head, tail,  
* Boxplots of Petal.Width by Species: R code: [iris.PetalWidth.r](https://github.com/chtsao/rgames2017/blob/master/iris.PetalWidth.r) 

# [R built-in data sets](http://www.sthda.com/english/wiki/r-built-in-data-sets)

