---
title: "Project Presentation"
date: 2021-12-21T06:08:38+08:00
draft: false
tags: [adm, team, project, presentation]
---

<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.ytimg.com%2Fvi%2FJstJFfdzi1I%2Fmaxresdefault.jpg&f=1&nofb=1" style="zoom: 50%;" />

# 期末報告 (課堂報告)

＊ 順序 18562437, qsc=questions, suggestions and comments.

1. 12/27. NBA數據分析 ([pres](), [rept](), [qsc](https://classroom.google.com/c/MzkxOTcwNDMyODk5/sa/NDUwMTk5NTY4Mzky/details)): 1.NBA 趣聞: 周郁森, 張博智, 張子恩, 鄭奕璿
3. 12/27. 成績與家庭背景相關分析 ([pres](), [rept](), [qsc](https://classroom.google.com/c/MzkxOTcwNDMyODk5/sa/NDUwMTk5Nzc0MzAy/details)): 5. 我要A: 黃培毓, 洪鈺玲
4. 1/3. 發文最佳時機 ([pres](), [rept](),[qsc]()): 6. 張秝穎, 吳雨瑾, 鍾宜玲
4. 1/3. TBA. ([pres](), [rept](),[qsc]()): 2. 別人恐懼我貪婪: 王靖翔, 許鴻源, 謝翼仲
7. 1/5. TBA. ([pres](), [rept]()): 3. 參貳柒: 陳逸陵, 蔡茗畯, 曾仁楷, 龔佑倫
8. 1/5. Covid-19. ([pres](), [rept]()): 7. 統軟一次過: 杜怡馨, 林昀潔, 徐思旻, 侯威志

#### 書面報告
* 個人：歐耀文、章賜恩
* 團隊
  * 音樂流量分析 ([pres](), [rept]()): 李洛廷, 林子齊, 陳冠綸, 劉承勛
  * 地震研究. ([pres](), [rept](),[qsc]()): 張祐華, 邱威誠, 姜秉勳

### 說明

* **課堂報告**：時間原則 25min, 硬體/Q&A: 3 min.。參考 Rmarkdown 模例[html](https://chtsao.gitlab.io/krg20/lalabear.html), [rmd](https://chtsao.gitlab.io/krg20/lalabear.Rmd)。
* **書面報告**：(2022) 1/16 23：59 前寄至我電郵信箱，以 pdf, html 檔為偏好格式。[架構參考](https://chtsao.gitlab.io/krg20/proj.template.nb.html)。
* 不在編組名單內小組/同學以繳交書面/電子報告為原則。如有高度課堂報告意願請儘快與我聯繫。如上列8小組沒有課堂報告意願，也請在表訂報告7天前告知，以便安排。
* （新）Kno 狗尾續貂, 試著幫已報告各組加上標題, 如果不適合或有另外更好建議，請讓我知道修正。另外，徵求各組同意後，我會將課堂報告以及最後書面報告放在課網連結，提供有興趣的閱者參考。

### R Markdown Links (New)

* [Lgatto's slide template](https://github.com/lgatto/slide-templates)
* [uslides by mkearney](https://github.com/mkearney/uslides)
* [R Markdown ioslides presentation](https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html) from [R Markdown: The Definitive Guide](https://bookdown.org/yihui/rmarkdown/)
* [Gallery from Rstudio](https://rmarkdown.rstudio.com/gallery.html)
* [Steve's R Markdown Templates](https://github.com/svmiller/svm-r-markdown-templates)

### 共同提醒與建議
* 一般
  * 提早準備，提前練習；自己/組內/找聽眾 Re 個幾次，注意流暢度，轉接，大約時間，節奏
  * 提早到場，確定硬、軟體相容性，電腦、麥克風、音響與其效果

* 內容
  * 聚焦：以 project 架構思考--在時間/人力/腦力限制下，研選/嚴選分析的主題探究程度與重點
  * 開始的問題提出，提示整個報告的重點；最後結論回應對問題的回答。
* 統計
  * EDA $\neq$ plot(data.frame) 或資料表列。以資料報告來說，比較更像一個初步資料呈現/summary. 
  * Raw data is more informative. 在 scatter plot 時，盡量做原始資料呈現，不先做 data summary (如僅畫平均的點)。
  * 統計/機器學習等方法可處理$Y$與多變數如$X_1, \cdots, X_p$之間的關係，不要僅限於單變數或簡單線性迴歸。 
  * Pie chart 不太適合作為比較或變化的呈現。比較的呈現要注意表格或統計圖形的挑選。
  * Scatterplot 無法以有效的統計模型說明時，也可回到統計圖表的根本，看圖理解，探尋可能的（不同）關聯。如 Alberto Cairo 建議，可以將圖形透過十字切割架構，劃分為類似於象限I,II, III, IV的四區，分別討論。更為進階的作法，可以透過如 active t-SNE 的方式，先將資料視覺化(二維圖)，再探究群內意義，群間關聯。
* 呈現
   * 精簡：少字大字，一張投影片一個主題（為原則）
   
   * 圖形的座標要清楚，事先思考選擇 X-Y軸座標
   
   * 避免不必要的翻頁，特別是連續翻多頁。如有必要，事先做好跳頁的設定。
   
   * 轉接頭等硬體設定事先準備確定 OK (可以課前先和系辦借)
   
     